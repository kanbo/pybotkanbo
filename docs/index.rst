.. . documentation master file, created by
   sphinx-quickstart on Mon Jun 16 15:54:39 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to .'s documentation!
=============================

Contents:

.. toctree::
   :maxdepth: 4

   bot
   botbrain
   conf
   conferror
   confman
   db
   event
   lite
   logger
   modules
   stats
   webwriter


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

